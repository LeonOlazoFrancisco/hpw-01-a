alumno={
  "numero_control" :["10160851","10160861","10160862","10160863","10160854","10160934"],
  "nombre":["Francisco" ,"Reinaldo" ,"Cristiano" ,"Arturo" ,"Pablo","Eduardo"],
  "apellidos":["Leon Olazo","Cruz Cruz","Perez Mendez","Lopez Pineda","Neruda Palacios","Mendez Dorigan"],
  "calificaciones":[10,10,10,9,8,9]
}

materia={
  "clave_materia":["scd01","scd02","scd03","scd04","scd05","scd06"],
  "nombre_materia":["Herramientas de Programacion web","Ecuaciones ","Gestion de Proyectos de Software","Quimica","Inteligencia Artificial","Programacion web"]
}

docente={
  "clave_docente":["A","B","C","D","E","F"],
  "nombre_docente":["Antonio","Mariano","Jose","Alan","Roberto","Richard"],
  "apellidos_docente":["Hernandez Blas","Perez Morales","Olazo Cruz","Morales Zarate","Pinacho Molina","Cruz Delgado"],
  "grado_academico":["Ingeniero","Licenciado","Doctorado","Licenciado","Licenciado","Doctorado"]
}

grupo={
  "clave":"ISA",
  "nombre":"GRP1",
  "docentes":["Antonio","Mariano","Jose","Alan","Roberto","Richard"],
  "alumnos":["10160851","10160861","10160862","10160863","10160854","10160934"],
  "materias":["Herramientas de Programacion web","Ecuaciones ","Gestion de Proyectos de Software","Quimica","Inteligencia Artificial","Programacion web"]
  }


  
function agregar_alumno(n_ctrl,nom,obj_alumno){
    var estado=false;
    var text="";
    for(var i=0;i<alumno["numero_control"].length;i++){
        if(n_ctrl==obj_alumno["numero_control"][i]){
            estado=true;
        }
    }
    
    if(estado === false){
        var pos = (obj_alumno["numero_control"].length+1);
        obj_alumno["numero_control"][pos]=n_ctrl;
        obj_alumno["nombre"][pos]=nom;
        text="Se agrego al alumno(a) "+nom+" con el numero de control "+n_ctrl;
    }
    else{
        text="El numero de control "+n_ctrl+" ya pertence a un alumno, Intente co otro";
    }
    return text;
}

agregar_alumno("10160897","pedro",alumno);


